import java.io.BufferedReader;
import java.io.FileReader;

public class ReadFile {

    public static void main(String args[] argv) {

        BufferedReader br = new BufferedReader(new FileReader("./readfile.py"));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            String everything = sb.toString();
        }
        finally {
            br.close();
        }
    }
}
