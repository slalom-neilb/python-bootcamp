import os, sys

rc_files = ["pythonrc", ".pythonrc"]

for rc_file in rc_files:
    if os.path.isfile(rc_file):
        rc_file_obj = open(rc_file, "r")
        for file_loc in rc_file_obj:
            file_loc = file_loc.rstrip('\n')
            if os.path.isdir(file_loc):
                sys.path.append(file_loc)

for path in sys.path:
    print(path)
