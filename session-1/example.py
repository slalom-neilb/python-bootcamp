#!/usr/bin/env python3

"""This is an example of how Python modules are structured"""
import os, sys # import comma delimited packages
import urllib.request as urllib_request # alias your imports
from subprocess import Popen # import selected static functions

class Example:
    """This documents the Generic class"""

    def __init__(self):
        """init a member variable"""
        self.bar="bar"

    def foo(self, an_argument):
        """This documents foo"""
        return "foo method has member %s called with arg %s" % (self.bar, an_argument)
    
    @staticmethod
    def static_foo():
        """This is a static class function
        I can add more than one line"""
        return 1

    def do_nothing(self):
        pass


class AnotherExample:
    """Yet another class in the module"""

    def bar(self, x="default"):
        """document bar"""
        return "bar called with %s" % x


def i_am_a_module_level_function():
    return "I can be called from this module"


if __name__ == "__main__":
    # Inspect stuff
    print("Module doc: %s" % __doc__)
    print("Class doc: %s" % Example.__doc__)
    print("Method doc: %s" % Example.foo.__doc__)

    # Create an instance and use Example
    example = Example()
    print("example.foo() returns: %s" % example.foo("fooey"))

    # Call a static method on Example
    print("Example.static_foo() returns: %s" % Example.static_foo())

    # Call a module function
    print("Module function: %s" % i_am_a_module_level_function())

