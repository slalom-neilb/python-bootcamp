from django.shortcuts import render
from django.http import HttpResponse
from django.core import serializers
import logging

from loggingexamples.models import Company

logger = logging.getLogger(__name__)

def home(request):
    
    logger.critical("critical")
    logger.error("error")
    logger.warning("warning")
    logger.info("info")
    logger.debug("debug")

    return render(request, 'home.html', {})

def allcompaniesjson(request):
    all_companies = Company.objects.all()
    data = serializers.serialize("json", all_companies, fields=("name"))

    return HttpResponse(data, mimetype='application/json')
    
def allcompaniesxml(request):
    all_companies = Company.objects.all()
    data = serializers.serialize("xml", all_companies, fields=("name"))

    return HttpResponse(data, mimetype='text/xml')
 
