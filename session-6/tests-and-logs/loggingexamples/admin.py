from django.contrib import admin

from loggingexamples.models import Company, Employee

admin.site.register(Company)
admin.site.register(Employee)
