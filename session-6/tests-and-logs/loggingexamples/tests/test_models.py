from django.test import TestCase

from loggingexamples.models import Company, Employee

class CompanyTestCase(TestCase):
    def setUp(self):
        Company.objects.create(name="Slalom LLC", number_employees=400)

    def testCompanyHasName(self):
        """Test the Company name"""
        slalom_company = Company.objects.get(name="Slalom LLC")

        self.assertEqual(slalom_company.name, "Slalom LLC")

    def testCompanyHasEmployees(self):
        """Test the Company has employees"""
        slalom_company = Company.objects.get(name="Slalom LLC")

        self.assertTrue(slalom_company.number_employees > 0)
