from django.test import TestCase

from loggingexamples import views 

class ViewTestCase(TestCase):

    def test_home(self):
        """Test the home page works"""
      
        response = views.home(None)

        self.assertTrue(response)

