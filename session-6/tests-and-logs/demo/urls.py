from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'loggingexamples.views.home', name='home'),
    url(r'^allcompaniesjson/', 'loggingexamples.views.allcompaniesjson'),
    url(r'^allcompaniesxml/', 'loggingexamples.views.allcompaniesxml'),
    url(r'^admin/', include(admin.site.urls)),
)
