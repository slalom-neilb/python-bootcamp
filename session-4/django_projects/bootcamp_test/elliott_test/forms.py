from django import forms

class UsernameForm(forms.Form):
	username = forms.EmailField()