from django.shortcuts import render
from django.http import HttpResponse

from elliott_test.forms import UsernameForm

# Create your views here.
def home(request):

	username = None
	if request.GET:
		myform = UsernameForm(request.GET)

		if myform.is_valid():
			username = myform.cleaned_data['username']


	return render(request, 'page.html', {'username': username})