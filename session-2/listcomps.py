# square every number in the list
squares = []
for i in range(0, 11):
    squares.append(i**2)

print(squares)


# list comprehension style
squares = [i**2 for i in range(0, 11)]
print(squares)


########################################


# nested loops
# find the numbers in common
a = [1, 3, 4, 8, 9]
b = [3, 5, 9, 22]

result = []
for x in a:
    for y in b:
        if x == y:
            result.append(y)

print(result)


# list comprehension style
in_common = [x for x in a for y in b if x == y]
print(in_common)
