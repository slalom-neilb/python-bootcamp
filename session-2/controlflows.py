# typical conitional
if True:
    do_something()
elif True:
    do_something()
else:
    do_something()

# continuing and breaking loops
for item in items:
    if True:
        break
    elif True:
        continue
