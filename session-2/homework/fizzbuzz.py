#!/usr/bin/env python3

"""Homework assignemnt for session 2"""

class FizzBuzz:
    """Awsome class that computes fizzy things"""

    def fizz_my_buzz(end=101):
        """Prints out fizzbuzz"""

        for i in range(0, end):
            if i % 3 == 0:
                print("fizz")
            elif i % 5 == 0:
                print("buzz")
            elif i % 3 and i % 5 == 0:
                print("fizzbuzz")
            else:
                print(i)


if __name__ == "__main__":
    print("Calling fizz_my_buzz with no arguments")
    FizzBuzz.fizz_my_buzz()

    print("\n\nCalling fizz_my_buzz up to 10")
    FizzBuzz.fizz_my_buzz(11)

