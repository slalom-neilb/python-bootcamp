# Loop over a range by step
# this does even numbers
for i in range(0, 11, 2):
    print(i)


# Loop with an index
for i, item in enumerate(["one", "two", "three"]):
    print(i, item)


# Reverse it
for i in reversed(range(0, 11)):
    print(i)


# Sort it back
for i in sorted(reversed(range(0, 11))):
    print(i)


# Loop over characters in a string
for char in "word in a sentense":
    print(char)


# Loop over keys
my_dict = {"one": 1, "two": 2}
for key in my_dict.keys():
    print(key, my_dict[key])


# Loop over key value pairs
for k, v in my_dict.items():
    print(k, v)



