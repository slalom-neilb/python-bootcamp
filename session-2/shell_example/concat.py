#!/usr/bin/env python3

import sys
import os

#make sure the user provided 2 arguments
assert len(sys.argv) == 3, "Usage: concat.py <input_directory> <output_filename>"

#open the output file for writing
output_filename = sys.argv[2]
with open(output_filename, 'w') as output_file:

	#go through every file in the provided input directory
	input_directory = sys.argv[1]
	for filename in os.listdir(input_directory):

		#if this file name ends with "js", concat it
		if(filename.endswith(".js")):

			full_filename = os.path.join(input_directory, filename)

			#open the inputfile for reading
			with open(full_filename, 'r') as input_file:
				output_file.write(input_file.read())

			#write a newline. python will convert it to the correct line separator for the platform
			output_file.write('\n')

