# optional args
def foo(required, optional=1):
    print(required, optional)

foo("required!")

# sequence args
def foo(arg1, *args):
    print(arg1, args)

foo(1, 2, 3)

# named args
def foo(arg1, **args):
    print(arg1, args)

foo(1, foo="foo", bar="bar")
