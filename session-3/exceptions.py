class A(Exception):
    pass

class B(Exception):
    pass

class C(Exception):
    pass


for exception in [A, A(), B, C]:
    try:
        raise exception
    except A:
        print("A")
    except B:
        print("B")
    except C:
        print("C")

