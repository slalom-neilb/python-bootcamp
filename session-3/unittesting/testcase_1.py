import unittest

class TestStuff(unittest.TestCase):
    
    def setUp(self):
        print("Setting stuff before a Test")

    def tearDown(self):
        print("Tearing stuff down after a Test")

    def testThatItWorks(self):
        self.assertEqual(True, True)
    
    #####################
    def foo():
        raise Exception
    #####################

    def testThatItRaises(self):
        with self.assertRaises(Exception):
            foo()

if __name__ == "__main__":
    unittest.main()
