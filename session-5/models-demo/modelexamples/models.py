from django.db import models

class Company(models.Model):
    name = models.CharField(max_length=30)
    number_employees = models.IntegerField()

    def __repr__(self):
        return "[{0}, {1}]".format(self.name, self.number_employees)

    def __str__(self):
        return self.__repr__()

class Employee(models.Model):
    company = models.ForeignKey(Company)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    date_of_birth = models.DateField(auto_now=True, auto_now_add=True)

    def __repr__(self):
        return "[{0}, {1}, {2}]".format(self.first_name, self.last_name, self.date_of_birth)

    def __str__(self):
        return self.__repr__();

