from django.contrib import admin

from modelexamples.models import Company, Employee

admin.site.register(Company)
admin.site.register(Employee)
